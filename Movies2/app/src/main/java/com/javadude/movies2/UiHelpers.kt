package com.javadude.movies2

import androidx.compose.runtime.*
import com.javadude.movies2.data.Actor
import com.javadude.movies2.data.ExpandedAppearance
import com.javadude.movies2.data.ExpandedRole
import com.javadude.movies2.data.Movie
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect

// ------------------------------------------------------------------------------------------------
// User interface helper functions/data
// ------------------------------------------------------------------------------------------------

/**
 * The status of an actor/movie being loaded (if one is selected) from the database
 */
enum class LoadStatus {
    NotSelected, Loading, Loaded
}

/**
 * Tracks data being loaded when a movie is being looked up by a @Composable function
 *
 * This one object will be remembered in the composable rather than having each field remembered,
 *   simplifying and reducing the redundant code in the display/edit functions
 */
@Immutable
data class MovieLoadState(
    val loadStatus: LoadStatus,
    val movieId: String? = null,
    val movie: Movie? = null,
    val cast: List<ExpandedRole>? = null,
)

/**
 * Tracks data being loaded when anm actor is being looked up by a @Composable function
 *
 * This one object will be remembered in the composable rather than having each field remembered,
 *   simplifying and reducing the redundant code in the display/edit functions
 */
@Immutable
data class ActorLoadState(
    val loadStatus: LoadStatus,
    val actorId: String? = null,
    val actor: Actor? = null,
    val filmography: List<ExpandedAppearance>? = null,
)

/**
 * Remembers a group of movie data being looked up as a single chunk.
 * Instead of a @Composable function needing to do several related remembers, we do them in this
 *   function and expose a single state that the caller can observe
 *
 * @param selection The current selection - changes will trigger movie/cast lookup
 * @param fetchers A holder for movie/actor/cast/filmography lookup functions
 */
@Composable
fun rememberMovieLoadState(
    selection: ListSelection,
    fetchers: Fetchers,
): State<MovieLoadState> {
    // set up a place to hold the movie load state - we'll return this to the caller
    val loadState = remember { mutableStateOf(MovieLoadState(LoadStatus.NotSelected)) }

    // get the movieId from the selection
    val movieId =
        when (selection) {
            NoListSelection, is MultiListSelection -> null
            is SingleListSelection -> selection.id
        }

    // whenever the movieId changes, launch a coroutine to lookup the movie/cast info
    //   (if a lookup were in progress, cancels its coroutine first)
    LaunchedEffect(movieId) {
        if (movieId == null) {
            loadState.value = MovieLoadState(LoadStatus.NotSelected)
        } else {
            val movie = fetchers.getMovie(movieId)
            // By using a flow for the cast instead of a synchronous get, the database will
            //   automatically send any updates for us to collect. This makes it automatic
            //   to get an updated list when an item is deleted, for example
            fetchers.getCastFlow(movieId).collect { cast ->
                loadState.value = MovieLoadState(LoadStatus.Loaded, movieId = movieId, cast = cast, movie = movie)
            }
        }
    }

    return loadState
}

/**
 * Remembers a group of actor data being looked up as a single chunk.
 * Instead of a @Composable function needing to do several related remembers, we do them in this
 *   function and expose a single state that the caller can observe
 *
 * @param selection The current selection - changes will trigger actor/filmography lookup
 * @param fetchers A holder for movie/actor/cast/filmography lookup functions
 */
@Composable
fun rememberActorLoadState(
    selection: ListSelection,
    fetchers: Fetchers,
): State<ActorLoadState> {
    // get the actorId from the selection
    val actorId =
        when (selection) {
            NoListSelection, is MultiListSelection -> null
            is SingleListSelection -> selection.id
        }

    // set up a place to hold the actor load state - we'll return this to the caller
    val loadState = remember { mutableStateOf(ActorLoadState(LoadStatus.NotSelected)) }

    // whenever the actorId changes, launch a coroutine to lookup the actor/filmography info
    //   (if a lookup were in progress, cancels its coroutine first)
    LaunchedEffect(actorId) {
        if (actorId == null) {
            loadState.value = ActorLoadState(LoadStatus.NotSelected)
        } else {
            loadState.value = ActorLoadState(LoadStatus.Loading, actorId = actorId)
            val actor = fetchers.getActor(actorId)
            // By using a flow for the filmography instead of a synchronous get, the database will
            //   automatically send any updates for us to collect. This makes it automatic
            //   to get an updated list when an item is deleted, for example
            fetchers.getFilmographyFlow(actorId).collect { filmography ->
                loadState.value = ActorLoadState(LoadStatus.Loaded, actorId = actorId, filmography = filmography, actor = actor)
            }
        }
    }

    return loadState
}

/**
 * A helper to hold onto common functions needed in the screens. We can set the lambdas up once
 *   and not have to have copies of the lambda code all over the place
 */
@Immutable
data class Fetchers(
    val getMovie: suspend (String) -> Movie?,
    val getActor: suspend (String) -> Actor?,
    val getCastFlow: (String) -> Flow<List<ExpandedRole>>,
    val getFilmographyFlow: (String) -> Flow<List<ExpandedAppearance>>,
)
