package com.javadude.movies2.data

import retrofit2.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.DELETE
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.PUT
import retrofit2.http.Path

const val BASE_URL = "http://10.0.2.2:8080" // host computer for emulator
    // NOTE: In a real app, you might allow the user to set this via settings

interface MovieApiService {
    @GET("movie")
    suspend fun getMovies(): Response<List<Movie>>

    @GET("actor")
    suspend fun getActors(): Response<List<Actor>>

    @GET("movie/{id}")
    suspend fun getMovie(@Path("id") id: String): Response<Movie> // movie or dummy movie to get around retrofit suspend bug

    @GET("actor/{id}")
    suspend fun getActor(@Path("id") id: String): Response<Actor> // actor or dummy actor to get around retrofit suspend bug

    @GET("actor/{id}/roles")
    suspend fun getRolesByActorId(@Path("id") id: String): Response<List<Role>>

    @GET("movie/{id}/roles")
    suspend fun getRolesByMovieId(@Path("id") id: String): Response<List<Role>>

    @GET("movie/{id}/cast")
    suspend fun getCast(@Path("id") id: String): Response<List<ExpandedRole>>

    @GET("actor/{id}/filmography")
    suspend fun getFilmography(@Path("id") id: String): Response<List<ExpandedAppearance>>

    @PUT("movie/{id}")
    suspend fun updateMovie(
        @Path("id") id: String,
        @Body movie: Movie
    ): Response<Int> // number updated

    @PUT("actor/{id}")
    suspend fun updateActor(
        @Path("id") id: String,
        @Body actor: Actor
    ): Response<Int> // number updated

    @PUT("role/{id}")
    suspend fun updateRoles(
        @Path("id") id: String,
        @Body role: Role
    ): Response<Int> // number updated

    // ...
    @POST("movie")
    suspend fun createMovie(@Body movie: Movie): Response<Movie>

    @POST("actor")
    suspend fun createActor(@Body actor: Actor): Response<Actor>

    @POST("role")
    suspend fun createRole(@Body role: Role): Response<Role>

    @DELETE("movie/{id}")
    suspend fun deleteMovie(@Path("id") id: String): Response<Int> // number deleted

    @DELETE("actor/{id}")
    suspend fun deleteActor(@Path("id") id: String): Response<Int> // number deleted

    @DELETE("role/{actorId}/{movieId}")
    suspend fun deleteRole(@Path("actorId") actorId: String, @Path("movieId") movieId: String): Response<Int> // number deleted

    @GET("reset")
    suspend fun resetDatabase(): Response<Int>

    companion object {
        fun create() =
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
                .create(MovieApiService::class.java)
    }
}

