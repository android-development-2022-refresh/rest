package com.javadude.movies2.data

import android.app.Application
import android.content.Context
import android.widget.Toast
import androidx.compose.foundation.layout.padding
import androidx.compose.material.*
import androidx.compose.runtime.Composable
import androidx.compose.runtime.Stable
import androidx.compose.runtime.remember
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.lifecycle.AndroidViewModel
import com.javadude.movies2.UndoManager
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.launch

@Composable
fun Screen(
    screenHelper: ScreenHelper,
    topBar: @Composable () -> Unit,
    content: @Composable (modifier: Modifier) -> Unit
) {
    AlertDialog(screenHelper = screenHelper)
    Scaffold(
        scaffoldState = screenHelper.scaffoldState,
        topBar = topBar,
        content = { content(Modifier.padding(it)) }
    )
}

@Composable
fun rememberScreenHelper(
    viewModel: ScreenViewModel,
    context: Context = LocalContext.current,
    scope: CoroutineScope = rememberCoroutineScope(),
    scaffoldState: ScaffoldState = rememberScaffoldState(),
) = remember(scope, viewModel, scaffoldState, context) {
    ScreenHelper(context, viewModel, scope, scaffoldState)
}

abstract class ScreenViewModel(application: Application) : AndroidViewModel(application) {
    private val _alertDialogData = MutableStateFlow<AlertDialogData?>(null)
    val alertDialogData: Flow<AlertDialogData?>
        get() = _alertDialogData

    val undoManager = UndoManager()

    fun setDialogData(
        title: String,
        text: String,
        cancelOnClickOutside: Boolean = false,
        showCancelButton: Boolean = true,
        onCancel: suspend () -> Unit = {},
        onOk: suspend () -> Unit = {},
    ) { _alertDialogData.value = AlertDialogData(title, text, cancelOnClickOutside, showCancelButton, onCancel, onOk) }

    fun clearDialogData() {
        _alertDialogData.value = null
    }

}

@Stable
class ScreenHelper(
    private val context: Context,
    private val viewModel: ScreenViewModel,
    internal val scope: CoroutineScope,
    internal val scaffoldState: ScaffoldState,
) {
    private var currentToast: Toast? = null
    val alertDialogData: Flow<AlertDialogData?>
        get() = viewModel.alertDialogData

    fun showDialog(
        title: String,
        text: String,
        cancelOnClickOutside: Boolean = false,
        showCancelButton: Boolean = true,
        onCancel: suspend () -> Unit = {},
        onOk: suspend () -> Unit = {},
    ) = viewModel.setDialogData(title, text, cancelOnClickOutside, showCancelButton, onCancel, onOk)

    fun showToast(
        message: String,
        long: Boolean = true,
    ) {
        scope.launch(Dispatchers.Main) {
            currentToast?.cancel()
            currentToast = Toast.makeText(context, message, if (long) Toast.LENGTH_SHORT else Toast.LENGTH_SHORT).apply {
                show()
            }
        }
    }

    fun showSnackbar(
        message: String,
        duration: SnackbarDuration = SnackbarDuration.Short,
    ) = showSnackbarImpl(message, duration, null, null)

    fun showSnackbar(
        message: String,
        action: String,
        duration: SnackbarDuration = SnackbarDuration.Short,
        onAction: (suspend () -> Unit)
    ) = showSnackbarImpl(message, duration, action, onAction)

    private fun showSnackbarImpl(
        message: String,
        duration: SnackbarDuration = SnackbarDuration.Short,
        action: String?,
        onAction: (suspend () -> Unit)?
    ) {
        scope.launch(Dispatchers.Default) {
            scaffoldState.snackbarHostState.currentSnackbarData?.dismiss()
            when (scaffoldState.snackbarHostState.showSnackbar(message, action, duration)) {
                SnackbarResult.Dismissed -> { } // do nothing on dismiss
                SnackbarResult.ActionPerformed -> onAction?.invoke()
            }
        }
    }

    fun dismissDialog() {
        viewModel.clearDialogData()
    }
}
