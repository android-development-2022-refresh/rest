package com.javadude.movies2.data

import androidx.room.Relation

// PLAIN kotlin class, NOT @Entity
data class ExpandedAppearance(
    val movieId: String,
    @Relation(
        parentColumn = "movieId",
        entityColumn = "id"
    )
    val movie: Movie,
    val character: String,
    val orderInCredits: Int
)